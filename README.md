# About
Projet sur une simulation d'un scan de code-barres avec une douchette.
Ici, on considère que le code-barres scanné est entré par un utilisateur dans un champ.

## Technologies
.NET Core - C#
SQLite
VB - WinForms

### Contributors
NICOLAS Dimitri
BORONAT Alexis

DUT 2A Informatique - Groupe 4
IUT Amiens - dép. Informatique