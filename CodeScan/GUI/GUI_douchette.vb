﻿Imports ClassLibrary
Public Class frm_douchette
    Dim first As Boolean
    Private Sub Btn_cb_Click(sender As Object, e As EventArgs) Handles btn_cb.Click
        Dim init_message = "Code-Barres : "
        Dim prod As Product

        prod = Product.createProduct(txt_cb.Text)
        lbl_enregistre.Visible = False

        DataBase.init()


        If (prod IsNot Nothing) Then '' Valid barcode
            lbl_valide.Text = init_message + "Valide"
            prod.saveProduct()
            If (prod.isProductRegistered()) Then '' Check the save in DB
                lbl_enregistre.Text = init_message + "Enregitré"
                lbl_enregistre.ForeColor = Color.ForestGreen
            Else
                lbl_enregistre.Text = init_message + "Non Enregistré"
                lbl_enregistre.ForeColor = Color.DarkRed
            End If
            lbl_enregistre.Visible = True
        Else
            lbl_valide.Text = init_message + "Non Valide"
        End If
        lbl_valide.Visible = True
    End Sub

    Private Sub Frm_douchette_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lbl_valide.Visible = False
        lbl_enregistre.Visible = False
        txt_cb.Text = "PFS-2019-5864"
        first = True
    End Sub

    Private Sub Txt_cb_Click(sender As Object, e As EventArgs) Handles txt_cb.Click
        If (first) Then
            txt_cb.Text = ""
            first = False
        End If
    End Sub

    Private Sub lbl_viewdb_Click(sender As Object, e As EventArgs) Handles lbl_viewdb.Click
        ViewDB_Form.ShowDialog()
    End Sub
End Class
