﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_douchette
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_cb = New System.Windows.Forms.TextBox()
        Me.lbl_title = New System.Windows.Forms.Label()
        Me.lbl_valide = New System.Windows.Forms.Label()
        Me.btn_cb = New System.Windows.Forms.Button()
        Me.lbl_enregistre = New System.Windows.Forms.Label()
        Me.lbl_viewdb = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txt_cb
        '
        Me.txt_cb.Location = New System.Drawing.Point(168, 124)
        Me.txt_cb.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txt_cb.Name = "txt_cb"
        Me.txt_cb.Size = New System.Drawing.Size(132, 22)
        Me.txt_cb.TabIndex = 0
        '
        'lbl_title
        '
        Me.lbl_title.AutoSize = True
        Me.lbl_title.Location = New System.Drawing.Point(16, 128)
        Me.lbl_title.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_title.Name = "lbl_title"
        Me.lbl_title.Size = New System.Drawing.Size(147, 17)
        Me.lbl_title.TabIndex = 1
        Me.lbl_title.Text = "Saisir le code-barres :"
        '
        'lbl_valide
        '
        Me.lbl_valide.AutoSize = True
        Me.lbl_valide.Location = New System.Drawing.Point(88, 249)
        Me.lbl_valide.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_valide.Name = "lbl_valide"
        Me.lbl_valide.Size = New System.Drawing.Size(138, 17)
        Me.lbl_valide.TabIndex = 2
        Me.lbl_valide.Text = "Code-barres : Valide"
        '
        'btn_cb
        '
        Me.btn_cb.Location = New System.Drawing.Point(339, 122)
        Me.btn_cb.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btn_cb.Name = "btn_cb"
        Me.btn_cb.Size = New System.Drawing.Size(100, 28)
        Me.btn_cb.TabIndex = 3
        Me.btn_cb.Text = "Envoyer"
        Me.btn_cb.UseVisualStyleBackColor = True
        '
        'lbl_enregistre
        '
        Me.lbl_enregistre.AutoSize = True
        Me.lbl_enregistre.Location = New System.Drawing.Point(88, 306)
        Me.lbl_enregistre.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_enregistre.Name = "lbl_enregistre"
        Me.lbl_enregistre.Size = New System.Drawing.Size(165, 17)
        Me.lbl_enregistre.TabIndex = 4
        Me.lbl_enregistre.Text = "Code-Barres : Enregistré"
        '
        'lbl_viewdb
        '
        Me.lbl_viewdb.AutoSize = True
        Me.lbl_viewdb.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lbl_viewdb.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_viewdb.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lbl_viewdb.Location = New System.Drawing.Point(354, 166)
        Me.lbl_viewdb.Name = "lbl_viewdb"
        Me.lbl_viewdb.Size = New System.Drawing.Size(74, 17)
        Me.lbl_viewdb.TabIndex = 5
        Me.lbl_viewdb.Text = "Voir BDD"
        '
        'frm_douchette
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(679, 441)
        Me.Controls.Add(Me.lbl_viewdb)
        Me.Controls.Add(Me.lbl_enregistre)
        Me.Controls.Add(Me.btn_cb)
        Me.Controls.Add(Me.lbl_valide)
        Me.Controls.Add(Me.lbl_title)
        Me.Controls.Add(Me.txt_cb)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frm_douchette"
        Me.Text = "Douchette"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txt_cb As TextBox
    Friend WithEvents lbl_title As Label
    Friend WithEvents lbl_valide As Label
    Friend WithEvents btn_cb As Button
    Friend WithEvents lbl_enregistre As Label
    Friend WithEvents lbl_viewdb As Label
End Class
