﻿Imports ClassLibrary
Public Class ViewDB_Form
    Private Sub ViewDB_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim products = DataBase.getAllProducts()
        Label1.Text = String.Format("Products ({0})", products.Count)
        ListBox1.Items.Clear()
        For Each product As Product In products
            ListBox1.Items.Add(String.Format("{0}-{1}-{2}", product.Type, product.Year, product.ID))
        Next
    End Sub
End Class