﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace ClassLibrary
{
    public static class DataBase
    {
        private const string DB_NAME = "db_products.db";
        private static SQLiteConnection connection;
        public static SQLiteConnection Connection
        {
            // Getter and Setter of the current connection etablished with SQLite database
            get
            {
                return connection;
            }
            set
            {
                if (value != null)
                    connection = value;
            }
        }
        public static void init()
        {
            /* init function : Initalize and prepare database to next saves (by creating the table)
             * No IN, No OUT
             * To call before saving any product in
             */
            SQLiteCommand query = null;
            Connection = connect();
            using (Connection)
            {
                query = new SQLiteCommand("CREATE TABLE IF NOT EXISTS Products(id INT, type VARCHAR(4), year INT, CONSTRAINT PK_BarCode PRIMARY KEY(id, type, year))", connection);
                query.ExecuteNonQuery();
            }
        }
        public static SQLiteConnection connect()
        {
            /* connect function : Try to connect to SQLite database (at the mentionned path : DB_NAME)
             * No IN
             * OUT : connection = SQLiteConnection 
             * 
             * Prefer calling this function in a using structure to guarantee the connection closing.
             */
            Connection = new SQLiteConnection(String.Format("Data Source={0};Version=3", DB_NAME));
            try
            {
                Connection.Open();
            }
            catch (SQLiteException)
            {
                SQLiteConnection.CreateFile(DB_NAME);
                Connection.Open();

            }
            return Connection;
        }
        public static List<Product> getAllProducts()
        {
            /* getAllProducts function : Get all the products datas saved in SQLite DB
             * No IN
             * OUT : result = List of Products
             * 
             * Called to display all the rows inserted in the table
             */
            List<Product> products = new List<Product>();
            using (Connection = connect())
            {
                SQLiteCommand query = new SQLiteCommand("SELECT * FROM Products", Connection);
                SQLiteDataReader results = query.ExecuteReader();
                Product product = null;
                if(results.HasRows)
                    while(results.Read())
                    {
                        product = new Product(results.GetString(1), results.GetInt32(2), results.GetInt32(0));
                        products.Add(product);
                    }
            }
            return products;
        }
    }
}
