﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace ClassLibrary
{
    public class Product
    {
        private string type;
        private const int MIN_YEAR = 1900;
        private const int MAX_YEAR = 2019;
        private int year;
        private int id;
        public int ID
        {
            // Getter and setter of product ID (third part of the barcode)
            set
            {
                id = value;
            }
            get
            {
                return id;
            }
        }
        public int Year
        {
            // Getter and setter of the creation year of the product (second part of the barcode)
            set
            {
                year = value;
            }
            get
            {
                return year;
            }
        }
        public string Type
        {
            // Getter and setter of product type (first part of the barcode)
            set
            {
                type = value;
            }
            get
            {
                return type;
            }
        }

        public Product(string type, int year, int id)
        {
            // Data checking perform out of this constructor
            this.type = type;
            this.year = year;
            this.id = id;
        }
        public static Product createProduct(string code_barres)
        {
            /* createProduct function : create a product object thanks to its barcode
             * IN : barcode = string
             * OUT : product = Product
             * Called like :  Product product = Product.createProduct("AAA-2019-1234");
             */
            string[] datas = code_barres.Split('-');
            try
            {
                Product res;
                res = new Product(datas[0], int.Parse(datas[1]), int.Parse(datas[2]));
                if (res.checkProductDatas()) // Checking parts of the barcode before returning object
                    return res;
                else
                    return null;
            }
            catch (FormatException) // One of the splitted parts is an invalid format
            {
                return null;
            }
            catch (IndexOutOfRangeException) // Unable to split three parts from the input barcode
            {
                return null;
            }
        }
        public void saveProduct()
        {
            /* saveProduct function : Save the datas product in SQLite database
             * No IN, no OUT
             * Called like :
             *  Product product = new Product("AAA-2019-1234");
             *  product.saveProduct();
             */
            using (DataBase.connect())
            {
                try
                {
                    SQLiteCommand query = new SQLiteCommand(String.Format("INSERT INTO Products (id, year, type) VALUES({0}, {1}, '{2}')", id, year, type), DataBase.Connection);
                    query.ExecuteNonQuery();
                }
                catch(SQLiteException)
                {
                    Console.WriteLine("Product already saved !");
                }
            }
        }
        private bool checkProductDatas()
        {
            /* checkProductDatas function : Check if the datas product fulfill all the conditions to be saved
             * No IN
             * OUT : result = bool
             * Called like :
             *  Product product = new Product("AAA-2019-1234");
             *  if(checkProductDatas())
             *  {
             *      product.saveProduct();
             *  }
             */
            if (type.Length != 3 || type.Count(c => char.IsUpper(c)) != 3) // Three CAPS letters
                return false;
            if (year < MIN_YEAR || year > MAX_YEAR)
                return false;
            if (id < 0 || id > 9999) // No more than 4 digits
                return false;
            return true;
        }
        public bool isProductRegistered()
        {
            /* isProductRegistered function : Check if a product is saved in the SQLite database
             * No IN
             * OUT : result = bool
             * Called like :
             * Product product = new Product("AAA-2019-1234");
             * if(!isProductRegistered())
             * {
             *      product.saveProduct();
             * }
             */
            using (DataBase.connect())
            {
                SQLiteCommand query = new SQLiteCommand(String.Format("SELECT id FROM Products WHERE id = {0} AND year = {1} AND type = '{2}'", id, year, type), DataBase.Connection);
                SQLiteDataReader results;
                results = query.ExecuteReader();
                return results.HasRows; // Return if some rows have been returned
            }
        }
    }
}
